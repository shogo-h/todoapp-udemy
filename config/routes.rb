Rails.application.routes.draw do
  resources :categories
  root "todos#index"
  resources :todos do
    member do
      patch 'status'
    end
  end
end
